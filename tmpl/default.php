<?php
/**
 * Instagram for Joomla! Module
 *
 * @author    TemplateMonster http://www.templatemonster.com
 * @copyright Copyright (C) 2012 - 2015 Jetimpex, Inc.
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 
 * 
 */
defined('_JEXEC') or die;
?>
<div class="mod_tm_instagram" id="module_<?php echo $module->id; ?>">
	<div class="row-fluid cols-3">
		<div id="instafeed"></div>
	</div>
</div>

<script type="text/javascript">
    var userFeed = new Instafeed({
        get: "<?php echo $SELECT_GET; ?>",
        tagName: "<?php echo $TAG_NAME; ?>",
        userId: "<?php echo $USER_ID; ?>",
        accessToken: "<?php echo $ACCESS_TOKEN; ?>",
        limit: "<?php echo $LIMIT; ?>"
    });
    userFeed.run();
</script>

﻿<?php
/**
 * Instagram for Joomla! Module
 *
 * @author    TemplateMonster http://www.templatemonster.com
 * @copyright Copyright (C) 2012 - 2015 Jetimpex, Inc.
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 
 * 
 */

defined('_JEXEC') or die;

$app 	  = JFactory::getApplication();	
$doc = JFactory::getDocument();
$document =& $doc;
$template = $app->getTemplate();
 
$document->addScript('modules/mod_tm_instagram/js/instafeed.min.js');

$LIMIT = $params->get('LIMIT');
$USER_ID = $params->get('USER_ID');
$TAG_NAME = $params->get('TAG_NAME');
$SELECT_GET = $params->get('SELECT_GET');
$ACCESS_TOKEN = $params->get('ACCESS_TOKEN');

require JModuleHelper::getLayoutPath('mod_tm_instagram', $params->get('layout', 'default'));